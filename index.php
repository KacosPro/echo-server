<?php
header('Content-Type: application/json');

$allowedMethods = [
  'GET',
  'POST',
  'PUT',
  'PATCH',
  'DELETE',
  // Add options header because of CORS
  'OPTIONS',
];

$responseCodes = [
  'badRequest' => 400,
  'methodNotAllowed' => 405,
];

$jsonData = file_get_contents("php://input");
$data = json_decode($jsonData);

if ($jsonData !== '' && $data === null) {
  http_response_code($responseCodes['badRequest']);
  $response = [
    'code' => $responseCodes['badRequest'],
    'data' => [
      'message' => 'Malformed JSON data',
    ],
  ];
  echo json_encode($response);
  return;
}

if (!in_array($_SERVER['REQUEST_METHOD'], $allowedMethods)) {
  http_response_code($responseCodes['methodNotAllowed']);
  $response = [
    'code' => $responseCodes['methodNotAllowed'],
    'data' => [
      'message' => 'Method not allowed',
    ],
  ];
  echo json_encode($response);
  return;
}

$response = [
  'method' => $_SERVER['REQUEST_METHOD'],
];
if (!empty($data)) {
  $response['data'] = $data;
}
echo json_encode($response);
